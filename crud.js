let http = require("http");

// Mock Data

let directory = [
    {
        name: "Brandon",
        email: "brandon@mail.com",
    },
    {
        name: "Jobert",
        email: "jobert@mail.com",
    },
];

http.createServer(function (req, res) {
    // Route for returning all items upon receiving get method
    if (req.url == "/users" && req.method == "GET") {
        res.writeHead(200, { "Content-type": "application/json}" });
        // Input has to be daa type STRING for the application to read the incoming data properly. convert JSON to string
        res.write(JSON.stringify(directory));
        // ends the response process
        res.end();
    }
    if (req.url == "/users" && req.method == "POST") {
        // declare and initialize reqBody variabe to an empty string
        // This wiil act as a placeholder for the data to be created later
        let reqBody = "";

        // This is where data insertion happends to our mock/dummy data
        req.on("data", function (data) {
            // assigns the data retrieve from the stream to reqBody
            reqBody += data;
        });
        // response end step - only runs after teh request has completely been set

        req.on("end", function () {
            console.log(typeof reqBody);

            // converts the strings to JSON
            reqBody = JSON.parse(reqBody);

            // create a new object representing the new mock data record

            let newUser = {
                name: reqBody.name,
                email: reqBody.email,
            };
            directory.push(newUser);
            console.log(directory);

            res.writeHead(200, { "Content-Type": "application/json" });

            res.write(JSON.stringify(newUser));

            res.end();
        });
    }
}).listen(3000);

console.log("CRUD is now running at port 3000");
